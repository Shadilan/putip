import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;

/**
 * Created by zhitnikov.bronislav on 20.11.2017.
 */
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class RequestIP {
    @Schedule(hour="*", minute="10", second="0", persistent=false)
    public void Hourly() throws SQLException, NamingException {
        String url="";

        URL obj= null;
        try {
            String app="FromHell";
            String code="Heaven";
            String pass="1ad0bbf7402b0fcd000418ed2f4cb06b";
            obj = new URL("https://getappip.000webhostapp.com/appl_ip_service.php?Operation=WriteIP&Application="+app+"&Code="+code+"&Password="+pass);
            HttpURLConnection connection= (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
